import Component from '@ember/component';
import EmberObject, { computed } from '@ember/object';
import { A } from '@ember/array';

export default Component.extend({
  actionObjects:null,
  actionMap: null,
  selectedValues: null,

  init() {
    this._super(...arguments);
    let actionMap = this.convertActionListToEmberArray();
    let actionObjects = A([]);

    for(let i = 0; i < actionMap.length; i++){
      let actionValueObject = EmberObject.create();
      let actionItem = actionMap[i];

      actionItem.set("actionValueObject", actionValueObject);

      if(typeof actionItem.get("default") !== "undefined"){
        actionValueObject.set("value", actionItem.get("default"));
      }
      actionValueObject.set("shouldUpdate",false);
      actionObjects.set(actionItem.get("action"),actionValueObject);
    }

     this.set("actionObjects",actionObjects);
     if(this.get("selectedValues") !==  null && this.get("selectedValues").length > 0){
       this.setInitialValues();
     }
     this.set("actionMap",actionMap);

  },

  updateValues(){
    let actionMap = this.get("actionMap");
    let actionValues = [];
    for(let i = 0; i < actionMap.length; i++){
      let actionItem = actionMap[i];
      let actionValueObject = actionItem.get("actionValueObject");

      if(actionValueObject.get("shouldUpdate") && typeof actionValueObject.get("value") !== "undefined"){
        actionValues.push({action: actionItem.get("action"), value: actionValueObject.get("value")});
      }
    }


    this.updateActions(actionValues);
  },

  setInitialValues(){
    let selectedValues = this.get("selectedValues");
    let actionObjects = this.get("actionObjects");

    for(let i = 0; i < selectedValues.length; i++){
      let value = selectedValues[i];
      let actionObject = actionObjects.get(value.action);
      actionObject.set("shouldUpdate",true);
      actionObject.set("value",value.value);
    }
  },

  convertActionListToEmberArray(){
    let actionList = this.get("actionList");
    let newList = [];

    actionList.forEach(function(listItem){
      let newListItem = A([]);
      for (var key in listItem) {
        newListItem.set(key,listItem[key]);
      }
      newList.push(newListItem);

    }.bind(this));
    return newList;
  },

  actions:{
    updateCheckbox(checkboxId){
      let actionObjects = this.get("actionObjects");
      let shouldInclude = $("#"+checkboxId)[0].checked;
      actionObjects.get(checkboxId).set("shouldUpdate",shouldInclude);
      this.updateValues();
    },

    updateValuesAction(){
      this.updateValues();
    }
  }
});
