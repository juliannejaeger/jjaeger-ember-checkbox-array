import Controller from '@ember/controller';

export default Controller.extend({
  actionsList: null,
  values: null,
  selectedValues: null,

  init() {
    this._super(...arguments);
    this.set("values",[]);
    this.set("selectedValues", [{action: "action1", value: "PENDING"}])
    let actions =
    [
      {action: 'action1',label: "Custom Label 1" , default: "ACTIVE",  options: [
            {label: "Active",value: 'ACTIVE' },
            {label: "Pending",value: 'PENDING' },
            {label: "Inactive",value: "INACTIVE"},
            {label: "Holding",value: "ONHOLD"}]},
            {action: 'action2', label: "Custom Label 2" , options: [
                  {label: "asdfasf",value: 'ACTIVE' },
                  {label: "asdfas",value: 'PENDING' },
                  {label: "Inacsfasdstive",value: "INACTIVE"},
                  {label: "sadfas",value: "ONHOLD"}]}
    ];

    this.set("actionsList",actions);
  },

  actions: {
    updateActions(actionValues){
      debugger;
    }
  }

});
